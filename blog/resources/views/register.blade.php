<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Buat Akun Baru!</title>
	</head>

	<body>

		<!-- heading-->
		<div>
			<h1>Buat Akun Baru!</h1>
			<h3>Sign Up Form</h3>
		</div>

		<!--form-->
		<form action="/welcome" method="POST" >
            @csrf
			<label for="first_name">First Name:</label><br><br>
				<input type="text" placeholder="Input Your First Name" id="first_name" name="first_name"><br><br>
			<label for="last_name">Last Name:</label><br><br>
				<input type="text" placeholder="Input Your Last Name" id="last_name"name="last_name"><br><br>
		
		<!--list-->
	
			<label>Gender:</label><br><br>
				<input type="radio" name="gender" value="0">Male<br>
				<input type="radio" name="gender" value="1">Female<br>
				<input type="radio" name="gender" value="2">Other<br><br>

			<label>Nationality:</label><br><br>
				<select>
					<optgroup>
						<option value="id">Indonesian</option>
						<option value="am">American</option>
						<option value="br">British</option>
					</optgroup>
				</select>
				<br><br>

			<label>Language Spoken:</label><br><br>
				<input type="checkbox" name="bahasa_user" value="0">Bahasa Indonesia<br>
				<input type="checkbox" name="bahasa_user" value="1">English<br>
				<input type="checkbox" name="bahasa_user" value="2">Other
				<br><br>

		
			<label for="bio_user">Bio:</label><br><br>
				<textarea cols="30" rows="10" id="bio_user" placeholder="Input Your Bio"></textarea><br>
			<input type="submit" value="Sign Up"><br><br>
		</form>

			
	</body>

</html>